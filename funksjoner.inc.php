﻿<?php

function connect(){//kobler til database (localhost ikke NTNU!!)
  $tilkobling = new mysqli ("localhost", "root", "", "moneyback"); //mysqli_connect("localhost", "root", "");
  return $tilkobling;
}

//logger brukeren ut
function logout(){
  session_destroy();

  echo "<script type='text/javascript'>alert('Logger deg ut.');</script>";
  header("refresh:2; url=index.php");
  exit();
}


function head(){ //lager header til hver side
  echo "
  <head>
    <meta charset='utf-8'>
    <link rel='stylesheet' type='text/css' href='style/style.css'>
    <link rel='stylesheet' media='screen and (min-width: 800px)' href='style/normscreen.css'>
    <link rel='stylesheet' media='screen and (max-width: 799px)' href='style/smallscreen.css'>
    <link rel='stylesheet' media='screen and (min-width: 1200px)' href='style/widescreen.css'>
    <title>Reisekost</title>
  </head>";
}

function navbar (){ //lager navbar til hver side
  echo "
  <nav>
      <ul>
        <li><a href='index.php'>Logg inn</a></li>
        <li><a href='registrer1.php'>Registrer ny reise</a></li>
        <li><a href='oversikt.php'>Din oversikt</a></li>
        <li><a href='rediger.php'>rediger en reise</a></li>
        <li><a href='statistikk.php'>se månedlig statistikk</a>
        <li><a href='logut.php'>logg ut</a></li><br>
        <form action='soek.php' method='post'>
        <li><input class='soekefelt' type='text' name='soek' value='søkefelt (stedsnavn)'></li>
        <li><input class='soekefelt' type='submit' value='søk'></li>
        </form>
      </ul>
  </nav>";
}

 ?>
