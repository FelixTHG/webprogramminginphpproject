<?php
include "funksjoner.inc.php";
head();
navbar();
if(isset($_SESSION['brukerid'])){
echo "<script type='text/javascript'>alert('Du ble ikke logget inn. Sender deg til innlogging');</script>";
header("refresh:2; url=index.php");
}
?>
<body>
  <section id="main">
    <h2>Logg inn:</h2>
    <form action="oversikt.php" method="post">
      <label for="epost">Din e-post: *</label><br>
      <input type="email" name="epost" id="epost"><br>
      <label for="passord">Passord:</label><br>
      <input type="password" name="passord" id="passord"><br>
      <input type="submit" value="logg inn" name="login">
    </form>
  </section>
</body>
</html>
