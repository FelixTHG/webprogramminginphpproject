﻿<?php
session_set_cookie_params(7200);
session_start();
include "funksjoner.inc.php";
//Steg 1: Tilkobling og valg av database
$tilkobling = connect();
//$tilkobling = mysqli_connect("localhost", "root", "");
//mysqli_select_db($tilkobling, "moneyback");
//Steg 2: Kjør en SQL-spørring mot databasen

//globale variabler
$fnavn;
$enavn;
$passord;
$paaloggetBruker;
$inputpassord;
$teller = 0;
$tabellteller = 0;

//til utregninger:
$totalkost = 0;
$gjsnittkost = 0;
$sumUtbetalt = 0;
$sumUtestående = 0;

//sjekker om $_POST['']-verdier for passord og epost
if(isset($_POST['passord']) && isset($_POST['epost'])){
$inputpassord = trim($_POST['passord']);
$paaloggetBruker = trim($_POST['epost']);
}

//sjekker om $_SESSION['']verdier for passord og epost allerede finnes (altså om bruker er innlogget allerede)
if (isset($_SESSION['passord']) && isset($_SESSION['epost'])) {
$inputpassord = $_SESSION['passord'];
$paaloggetBruker = $_SESSION['epost'];
}


//SELECT * FROM abonnent WHERE epost = "dummy@dummy.dum"
if(isset($inputpassord) && isset($paaloggetBruker)){
$sql = "SELECT * FROM users WHERE epost = '" . $paaloggetBruker . "';";
//skriver ut spørringen, kan med fordel fjernes hvis alt fungerer
//echo $sql;
$resultat = mysqli_query($tilkobling, $sql);

//fyller session[] info fra database, dersom passord og brukernavn stemmer med en rad.
while($rad = mysqli_fetch_array($resultat) ) {
$brukerid = $rad['id'];
$epost = $rad['epost'];
$passord = $rad['passord'];
if ($epost == $paaloggetBruker && $inputpassord == $passord){
  $fornavn = $rad['fnavn'];
  $etternavn = $rad['enavn'];
  $_SESSION['fnavn'] = $fornavn;
  $_SESSION['enavn'] = $etternavn;
  $_SESSION['passord'] = $passord;
  $_SESSION['epost'] = $epost;
  $_SESSION['brukerid'] = $brukerid;
}
}
}
?>
  <section name="main">
  <?php
    head();
    navbar();
    //dersom session['brukerid'] ikke er satt faar en ikke tilgang til noe av koden under.
    if(isset($_SESSION['brukerid'])){
    echo "<h1>Velkommen $fornavn</h1>";
//    echo "<h2>Du har $teller ubetalte reiser</h2>"; Kommer for tidlig i koden
    echo "<h2> Dette er din oversikt:</h2>";
    echo "<hr />";

    //henter data fra db
    $sqlx = "SELECT * FROM travels WHERE brukerid = $brukerid";
    $resultatx = mysqli_query($tilkobling, $sqlx);

//lager table head
    echo "<table><br>
    <tr>
    <th>reise-id</th>
    <th>dato</th>
    <th>reisemål</th>
    <th>agenda</th>
    <th>kostnad</th>
    <th>utbetalt</th>
    </tr>
    ";


    //Steg 3: Behandle resultatet med PHP og HTML
    //setter resultat av spoerring inn i tabellen
    while($radx = mysqli_fetch_array($resultatx) ) {
    $reiseid = $radx['id'];
    $dato = $radx['dato'];
    $reisemaal = $radx['reisemaal'];
    $agenda = $radx['agenda'];
    $kostnad = $radx['kostnad'];
    $teller++;
    $totalkost += $kostnad;
    if($radx['utbetalt'] == 0){
      $utbetalt = "Nei";
      $sumUtestående += $kostnad;
    }
    else {
    $utbetalt = "Ja";
    $sumUtbetalt += $kostnad;
    }
    if($tabellteller%2 == 0){ //denne if-elsen lager tabellelementer med en klasse for å fargelegge annenhver.
        $tabellteller++;
        echo "<tr>";
        echo "<td class='bg_highlight'>$reiseid</td>";
        echo "<td class='bg_highlight'>$dato</td>";
        echo "<td class='bg_highlight'>$reisemaal</td>";
        echo "<td class='bg_highlight'>$agenda</td>";
        echo "<td class='bg_highlight'>$kostnad</td>";
        echo "<td class='bg_highlight'>$utbetalt</td>";
        echo "</tr>";
    }
    else{
      $tabellteller++;
      echo "<tr>";
      echo "<td>$reiseid</td>";
      echo "<td>$dato</td>";
      echo "<td>$reisemaal</td>";
      echo "<td>$agenda</td>";
      echo "<td>$kostnad</td>";
      echo "<td>$utbetalt</td>";
      echo "</tr>";
    }
    }
    //aner faktisk ikke hvorfor dette kommer over tabellen på min lokale løsning, men det setter jeg egentlig pris på.
    echo "
    <h3>noen utregninger:</h3>
    <ol>
    <li>Antall reiser er: <strong>$teller</strong></li>
    <li>Totalkostnaden av reisene er: <strong>$totalkost</strong> kr</li>
    <li>Gjennomsnittskosten på en reise er: <strong>" . $totalkost/$teller . "</strong> kr</li>
    <li>Sum utbetalte kostnader er: <strong>$sumUtbetalt</strong> kr</li>
    <li>Sum utestående kostnader er: <strong>$sumUtestående</strong> kr</li>
    </ol>
    ";
  }
  else {
      session_destroy();
      echo "<script type='text/javascript'>alert('Du ble ikke logget inn. Sender deg til innlogging');</script>";
      header("refresh:2; url=index.php");
      exit;
  }
    //databasetilkoblingen lukkes
    $tilkobling->close();
       ?>
  </section>
</body>
</html>
