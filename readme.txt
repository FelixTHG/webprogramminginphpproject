﻿Om man klipper og limer all teksten fra filen "databaseOppsett.txt" vil man få en database 
med test-verdier fra prosjekt-teksten.
Den har 7 reiser i en tabell travels, og 1 bruker i en tabell users.
Den vil altså kun inneholde en bruker som har e-post = "p.k@epost.no" og passord = "gulrot".
Brukerens fornavn er prove, etternavn kanin.
Bruker id=1, og er eier av alle dummy-reisene.

Du kan legge inn flere reiser i databasen via registrer1.php etter du har logget 
deg inn med proeve kanin brukeren.

Etter du har logget inn, via index.php vil du bli dirigert til en oversikt over alle dine reiser. (oversikt.php)
Etter du har registrert en ny reise, vil du bli dirigert til oversikten. (oversikt.php)
Index.php er login-siden.

funksjoner.inc.php er funksjoner som kanskje blir utvidet senere hvis jeg oppdager at funksjonalitet er 
hensiktsmessig å gjenbruke. Til nå inneholder den blant annet header og nav-bar. 

Logut.php logger deg ut. 

Registrer1.php inneholder et skjema for å registrere en reise. 
Registrer.php behandler informasjonen som er sendt i registrer1.php.

rediger.php lar brukeren velge en reise å oppdatere, mensoppdater.php oppdaterer databasen.

soek.php tar imot resultatet av et stedsnavn/destinasjon skrevet i søkefeltet i nav-baren.

Nettstedet funker helt fint for meg akkurat nå, så jeg håper virkelig det gjør det for deg også.
Jeg bruker nyeste firefox akkurat nå til testing. Google chrome virket også så det ut som.

OBS! DATABASE FUNGERER IKKE PÅ folk.ntnu.no FORDI JEG IKKE HAR FÅTT DATABASE FRA ORAKELTJENESTEN.
Dermed må du nok sette det opp lokalt på localhost, dette beklager jeg veldig. 
Her er lenken til mitt arbeid som ligger paa nett. (ingenting avhengig av database virker her dessverre!)
http://folk.ntnu.no/ftgrimsr/PHPPROJ/
