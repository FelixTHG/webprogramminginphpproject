<?php
//include?
session_start();
include "funksjoner.inc.php";

head();
navbar();

//sjekk om bruker er pålogget
if(isset($_SESSION['epost']) && isset($_SESSION['passord']) && isset($_SESSION['brukerid'])){

  //koble til database
  $tilkobling = connect();


//sjekk om $_POST[reiseid] er satt og finn reisen. (kanskje sjekk om tall også)
if(isset($_POST['reiseid'])){
  $sql = "SELECT * FROM travels WHERE id = '" . $_POST['reiseid'] . "' AND brukerid = '" . $_SESSION['brukerid'] . "';";
  $resultatx = mysqli_query($tilkobling, $sql);

  while($radx = mysqli_fetch_array($resultatx) ) {
  $reiseid = $radx['id'];
  $dato = $radx['dato'];
  $reisemaal = $radx['reisemaal'];
  $agenda = $radx['agenda'];
  $kostnad = $radx['kostnad'];
  if($radx['utbetalt'] == 0){
     $utbetalt = "";
     $ikkeutbetalt = "checked";
   }
  else {
    $utbetalt = "checked";
    $ikkeutbetalt = "";
  }
  echo "
  <form action='oppdater.php' method='post'>
    <label for='dato'>dato for reise *</label><br>
    <input type='date' name='dato' id='dato' value='" . $dato . "'><br>
    <label for='reisemaal'>reisemål *</label><br>
    <input type='text' name='reisemaal' id='reisemaal' value='" . $reisemaal . "'><br>
    <label for='agenda'>agenda *</label><br>
    <input type='text' name='agenda' id='agenda' value='" . $agenda . "'><br>
    <label for='kostnad'>kostnad *</label><br>
    <input type='number' name='kostnad' id='kostnad' value='" . $kostnad . "'><br>
    <label for='utbetalt'>utbetalt *</label><br>
    <input type='radio' name='utbetalt' id='utbetalt' value='1' checked='" . $utbetalt . "'>Ja<br>
    <input type='radio' name='utbetalt' id='utbetalt' value='0' checked='" . $ikkeutbetalt . "'>Nei<br>
    <input type='hidden' name='reiseid' value='" . $_POST['reiseid'] . "'>
    <input type='submit' value='registrer' name='registrer'><br>
  </form>
  ";
  }
  if(empty($reiseid)){
    echo "<script type='text/javascript'>alert('Her skjedde noe uventet, og vi fant ikke reisen.');</script>";
    header("refresh:0; url=rediger.php");
    }

}

//hvis $_POST[reiseid] ikke er satt, la brukeren sende en reise-id
else{
  echo "Vennligst skriv et reise-id nummer. Husk at du kan bruke søkefunksjonen til å finne rett reise.";
  echo "<form action='rediger.php' method='post'>
          <input type='number' name='reiseid'>
          <input type='submit' value='hent reise'>
        </form>
  ";
}



//hvis $_POST[] ikke er satt
//skrive-felt der en legger inn id


//hvis $_POST[] er satt
//hent reise med x id
//sett verdier inn i skrivefeltene fra registrer1.php
$tilkobling->close();
} // slutt if om bruker er pålogget
else {
  session_destroy();
  echo "<script type='text/javascript'>alert('Du ble ikke logget inn. Sender deg til innlogging');</script>";
  header("refresh:2; url=index.php");
  exit;
}

?>
