<?php
session_set_cookie_params(7200);
session_start();
include "funksjoner.inc.php";
head();
navbar();

?>
  <section id="main">
    <h2>Registrer reise:</h2>
    <h3></h3>

    <form action="registrer.php" method="post">
      <label for="dato">dato for reise *</label><br>
      <input type="date" name="dato" id="dato"><br>
      <label for="reisemaal">reisemål *</label><br>
      <input type="text" name="reisemaal" id="reisemaal"><br>
      <label for="agenda">agenda *</label><br>
      <input type="text" name="agenda" id="agenda"><br>
      <label for="kostnad">kostnad *</label><br>
      <input type="number" name="kostnad" id="kostnad"><br>
      <label for="utbetalt">utbetalt *</label><br>
      <input type="radio" name="utbetalt" id="utbetalt" value="1">Ja<br>
      <input type="radio" name="utbetalt" id="utbetalt" value="0">Nei<br>
      <input type="submit" value="registrer" name="registrer"><br>
    </form>
  </section>
</body>
</html>
