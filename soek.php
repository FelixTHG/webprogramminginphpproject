<?php
//include?
session_start();
include "funksjoner.inc.php";

head();
navbar();


$tabellteller = 0;

//sjekk om bruker er pålogget
if(isset($_SESSION['epost']) && isset($_SESSION['passord']) && isset($_SESSION['brukerid'])){

  //koble til database
  $tilkobling = connect();

//sjekk om $_POST[reiseid] er satt og finn reisen. (kanskje sjekk om tall også)
//hvis $_POST[] er satt
//hent reise med x id
//sett verdier inn i skrivefeltene fra registrer1.php
if(isset($_POST['soek'])){
  //SELECT * FROM travels WHERE reisemaal = 'Fornebu' AND brukerid = '1'; --ønsket spørring
  echo "<h2>Søkeresultater: </h2>";
  $soek = trim($_POST['soek']);
  $sql = "SELECT * FROM travels WHERE reisemaal = '" . $soek . "' AND brukerid = '" . $_SESSION['brukerid'] . "';";
  $resultatx = mysqli_query($tilkobling, $sql);

  //lager table head
      echo "<table><br>
      <tr>
      <th>reise-id</th>
      <th>dato</th>
      <th>reisemål</th>
      <th>agenda</th>
      <th>kostnad</th>
      <th>utbetalt</th>
      </tr>
      ";


      //Steg 3: Behandle resultatet med PHP og HTML
      //setter resultat av spoerring inn i tabellen
      while($radx = mysqli_fetch_array($resultatx) ) {
      $reiseid = $radx['id'];
      $dato = $radx['dato'];
      $reisemaal = $radx['reisemaal'];
      $agenda = $radx['agenda'];
      $kostnad = $radx['kostnad'];
      if($radx['utbetalt'] == 0) $utbetalt = "Nei";
      else $utbetalt = "Ja";
      if($tabellteller%2 == 0){ //denne if-elsen lager tabellelementer med en klasse for å fargelegge annenhver.
          $tabellteller++;
          echo "<tr>";
          echo "<td class='bg_highlight'>$reiseid</td>";
          echo "<td class='bg_highlight'>$dato</td>";
          echo "<td class='bg_highlight'>$reisemaal</td>";
          echo "<td class='bg_highlight'>$agenda</td>";
          echo "<td class='bg_highlight'>$kostnad</td>";
          echo "<td class='bg_highlight'>$utbetalt</td>";
          echo "</tr>";
      }
      else{
        $tabellteller++;
        echo "<tr>";
        echo "<td>$reiseid</td>";
        echo "<td>$dato</td>";
        echo "<td>$reisemaal</td>";
        echo "<td>$agenda</td>";
        echo "<td>$kostnad</td>";
        echo "<td>$utbetalt</td>";
        echo "</tr>";
      }
      }
  if(empty($reiseid)){ //gir en advarsel dersom reise-id ikke er satt
    echo "<script type='text/javascript'>alert('Her skjedde noe uventet, og vi fant ikke reisen.');</script>";
    header("refresh:0; url=rediger.php");
    }

}

//hvis $_POST[reiseid] ikke er satt, la brukeren sende en reise-id
else{
  echo "Du kan prøve igjen. Pass på at du skriver navnet helt rett.";
}



$tilkobling->close();
} // slutt if om bruker er pålogget
else {
  session_destroy();
  echo "<script type='text/javascript'>alert('Du ble ikke logget inn. Sender deg til innlogging');</script>";
  header("refresh:2; url=index.php");
  exit;
}

?>
