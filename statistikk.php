<?php
//include?
session_start();
include "funksjoner.inc.php";

head();
navbar();

echo "Vennligst skriv en måned og et årstall.";
echo "<form action='statistikk.php' method='post'>
        <label for='maaned'>måned</label>
        <input type='number' name='maaned' max='12' min='1'>
        <label for='aar'>år</label>
        <input type='number' name='aar'>
        <input type='submit' value='hent reise'>
      </form>
";

//sjekk om bruker er pålogget
if(isset($_SESSION['epost']) && isset($_SESSION['passord']) && isset($_SESSION['brukerid'])){

  //globale variabler
  $teller = 0;
  $tabellteller = 0;

  //til utregninger:
  $totalkost = 0;
  $sumUtbetalt = 0;
  $sumUtestående = 0;
  $gjsnittkost = 0;
  //koble til database
  $tilkobling = connect();


//sjekk om $_POST[maaned] && $_POST[aar] er satt og finn reiser. (kanskje sjekk om tall også)
if(isset($_POST['maaned']) && isset($_POST['aar'])){
//  $maaned = $_POST['maaned'];
//  $aar = $_POST['aar'];

  //2 felt, en med måned en med år
  $sql = "SELECT * FROM travels WHERE MONTH(dato) = '" . $_POST['maaned'] . "' AND YEAR(dato) = '" . $_POST['aar'] . "' AND brukerid = '" . $_SESSION['brukerid'] . "';";
  $resultatx = mysqli_query($tilkobling, $sql);

  //lager table head
      echo "<table><br>
      <tr>
      <th>reise-id</th>
      <th>dato</th>
      <th>reisemål</th>
      <th>agenda</th>
      <th>kostnad</th>
      <th>utbetalt</th>
      </tr>
      ";


      //Steg 3: Behandle resultatet med PHP og HTML
      //setter resultat av spoerring inn i tabellen
      while($radx = mysqli_fetch_array($resultatx) ) {
      $reiseid = $radx['id'];
      $dato = $radx['dato'];
      $reisemaal = $radx['reisemaal'];
      $agenda = $radx['agenda'];
      $kostnad = $radx['kostnad'];
      $teller++;
      $totalkost += $kostnad;
      if($radx['utbetalt'] == 0){
        $utbetalt = "Nei";
        $sumUtestående += $kostnad;
      }
      else {
      $utbetalt = "Ja";
      $sumUtbetalt += $kostnad;
      }
      if($tabellteller%2 == 0){//denne if-elsen lager tabellelementer med en klasse for å fargelegge annenhver.
          $tabellteller++;
          echo "<tr>";
          echo "<td class='bg_highlight'>$reiseid</td>";
          echo "<td class='bg_highlight'>$dato</td>";
          echo "<td class='bg_highlight'>$reisemaal</td>";
          echo "<td class='bg_highlight'>$agenda</td>";
          echo "<td class='bg_highlight'>$kostnad</td>";
          echo "<td class='bg_highlight'>$utbetalt</td>";
          echo "</tr>";
      }
      else{
        $tabellteller++;
        echo "<tr>";
        echo "<td>$reiseid</td>";
        echo "<td>$dato</td>";
        echo "<td>$reisemaal</td>";
        echo "<td>$agenda</td>";
        echo "<td>$kostnad</td>";
        echo "<td>$utbetalt</td>";
        echo "</tr>";
      }
      }
      if($teller !== 0) $gjsnittkost = $totalkost/$teller; //sjekker om en ender opp med å dele på 0
      else {
        $gjsnittkost = 0;
        echo "Tabellen blir tom ettersom det ikke er noen registrerte reiser i dette tidsløpet.";
      }
      //lister ut statistikken
      echo "
        <ol>
          <li>Antall reiser i måneden " . $_POST['maaned'] . " i år " . $_POST['aar'] . " er: <strong>$teller</strong></li>
          <li>Totalkostnaden for samme periode er: <strong>$totalkost</strong></li>
          <li>En gjennomsnittsreise koster: <strong>$gjsnittkost</strong></li>
          <li>Utestående beløp for perioden er: <strong>$sumUtestående</strong></li>
          <li>Utbetalt beløp for perioden er: <strong>$sumUtbetalt</strong></li>
        </ol>
      ";
}


$tilkobling->close();
} // slutt if om bruker er pålogget
else {
  session_destroy();
  echo "<script type='text/javascript'>alert('Du ble ikke logget inn. Sender deg til innlogging');</script>";
  header("refresh:2; url=index.php");
  exit;
}

?>
